//Мультипоточная сортировка выбором
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <thread>
#include <pthread.h>
#include <sched.h>
#include <time.h>
using namespace std;
const int arraySize = 100;
//g++  Osis_lab4.cpp -pthread -std=gnu++11
int firstArray[100], secondArray[100], thirdArray[100];
void func1() {
	//Сортировка первого и второго массивов
	int minInFirstArray = 0; int minInSecondArray = 0;
	//Первый массив
	for (int i = 0; i < arraySize - 1; i++){
		cout << "_1" << endl;
		minInFirstArray = i;
		for (int j = i + 1; j < arraySize; j++){
			if (firstArray[j] < firstArray[minInFirstArray]){
				minInFirstArray = j;
			}
		}
		swap(firstArray[i], firstArray[minInFirstArray]);
	}
	//Второй массив
	for (int i = 0; i < arraySize - 1; i++){
		cout << "__2" << endl;
		minInSecondArray = i;
		for (int j = i + 1; j < arraySize; j++){
			if (secondArray[j] < secondArray[minInSecondArray]){
				minInSecondArray = j;
			}
		}
		swap(secondArray[i], secondArray[minInSecondArray]);
		sched_yield();
	}
}
void func2() {
	//Сортировка третьего массива
	int minInThirdArray = 0;
	for (int i = 0; i < arraySize - 1; i++){
		cout << "___3" << endl;
		minInThirdArray = i;
		for (int j = i + 1; j < arraySize; j++){
			if (thirdArray[j] < thirdArray[minInThirdArray]){
				minInThirdArray = j;
			}
		}
		swap(thirdArray[i], thirdArray[minInThirdArray]);
		sched_yield();
	}
}

int main(void){
	srand(time(NULL));
	//int arraySize = 0;
	//cout << "Введите количество элементов в массивах: "; cin >> arraySize;
	//cout << "Созданы массивы из " << arraySize << " элемента(ов)" << endl;
	/*int *firstArray  = new int(arraySize);
	int *secondArray = new int(arraySize);
	int *thirdArray  = new int(arraySize);*/
	pthread_t ptid1, ptid2;
	pthread_attr_t attr;
	struct sched_param param;
	pthread_attr_init(&attr);
	pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
	pthread_attr_setschedpolicy(&attr, SCHED_RR);
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	param.sched_priority = 9;
	pthread_attr_setschedparam(&attr, &param);
	cout << "Первый массив: ";
	for (int i = 0; i < arraySize; i++){
		firstArray[i] = rand()%20;
		cout << firstArray[i] << " ";
	} 
	cout << endl << "Второй массив: ";
	for (int i = 0; i < arraySize; i++){
		secondArray[i] = rand()%20;
		cout << secondArray[i] << " ";
	} 
	cout << endl << "Третий массив: ";
	for (int i = 0; i < arraySize; i++){
		thirdArray[i] = rand()%20;
		cout << thirdArray[i] << " ";
	} cout << endl;
	thread thread1(func1, firstArray, secondArray, arraySize); thread thread2(func2, thirdArray, arraySize);
	thread1.join(); thread2.join();
	cout << endl << "Отсортированный первый массив: "; for (int i = 0; i < arraySize; i++){ cout << firstArray[i] << " ";  }
	cout << endl << "Отсортированный второй массив: "; for (int i = 0; i < arraySize; i++){ cout << secondArray[i] << " "; }
	cout << endl << "Отсортированный третий массив: "; for (int i = 0; i < arraySize; i++){ cout << thirdArray[i] << " ";  }
	cout << endl <<  "done" << endl;
	//delete firstArray, secondArray, thirdArray; 
	/*pthread_create(&ptid1, &attr, &func1, NULL);
	if (ptid1) {cout<< endl << "Создал первый поток" << endl;}
	param.sched_priority = 8;
	pthread_attr_setschedparam(&attr, &param);
	pthread_create(&ptid2, &attr, &func2, NULL);
	if (ptid2) {cout << endl << "Создал второй поток" << endl;}
	thread thread1(func1);
	thread thread2(func2);
	pthread_join(ptid1, NULL);
	pthread_join(ptid2, NULL);	
	cout << endl << "Отсортированный первый массив: "; for (int i = 0; i < arraySize; i++){ cout << firstArray[i] << " ";  }
	cout << endl << "Отсортированный второй массив: "; for (int i = 0; i < arraySize; i++){ cout << secondArray[i] << " "; }
	cout << endl << "Отсортированный третий массив: "; for (int i = 0; i < arraySize; i++){ cout << thirdArray[i] << " ";  }
	cout << endl << "done" << endl;
	cout << endl;
*/
}